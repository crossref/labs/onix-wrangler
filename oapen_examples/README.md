## OAPEN Example scripts

These example VBA scripts were provided by [OAPEN](https://www.oapen.org/) in order to illustrate some of the ONIX metadata cleanup processes they have to undertake when they recieve data from publishers.

