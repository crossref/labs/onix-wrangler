from io import BytesIO
import streamlit as st
from onix2csv import onix_to_df

import pandas as pd


results = st.container()


def process():
    with st.spinner(text="Processing..."):
        for entry in st.session_state.onix:
            df = onix_to_df(entry.getvalue())
            st.session_state["processed"].append(df)


if "processed" not in st.session_state:
    st.session_state["processed"] = []


with open("README.md", "r") as f:
    st.markdown(f.read())

with st.expander("Show xpaths that are used to extract data from the ONIX file"):
    with open("settings.py", "r") as f:
        st.code(f.read(), language="python")


st.file_uploader(
    "Upload ONIX files",
    type=["onx", "xml"],
    accept_multiple_files=True,
    key="onix",
    help=None,
)
if len(st.session_state.onix) > 0:
    st.button(
        "Convert to intermediate format",
        key="convert",
        on_click=process,
        args=None,
        kwargs=None,
    )

if len(st.session_state.processed) > 0:
    st.header("Results")
    for file_no, df in enumerate(st.session_state.processed):
        fn = st.session_state.onix[file_no].name
        base_name = fn.split(".")[0]
        st.subheader(f"File: {fn}")
        edited_data = st.experimental_data_editor(df)
        st.download_button(
            label="Download CSV",
            data=edited_data.to_csv(),
            file_name=f"{base_name}.csv",
            mime="text/csv",
        )
        st.download_button(
            label="Download JSON",
            data=edited_data.to_json(orient="records"),
            file_name=f"{base_name}.json",
            mime="application/json",
        )
