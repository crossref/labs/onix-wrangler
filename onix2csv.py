import sys
import logging
import typer
import pandas as pd
from lxml import etree
from io import BytesIO
#from rich_dataframe import prettify
from settings import XPATHS_OF_INTEREST


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def folded_xpath(parent, xpath, namespaces):
    """
    If the xpath query returns no results, try again with the query in lowercase

    :param parent: the parent element to search from
    :param xpath: the xpath to the element you want to extract
    :param namespaces: a dictionary of namespaces used in the XML document
    :return: A list of elements
    """
    logger.info(f">>>> xpath: {xpath}")
    res = parent.xpath(xpath, namespaces=namespaces)
    if len(res) > 0:
        return res
   
    logger.info(f"xpath_lower: {xpath.lower()}")
    return parent.xpath(xpath.lower(), namespaces=namespaces)


def get_ns_dec(root):
    """
    It returns a tuple of the namespace prefix and a dictionary of the namespace prefix and its URI

    :param root: the root element of the XML file
    :return: a tuple of two values. The first value is a string that is the namespace prefix. The second
    value is a dictionary that contains the namespace prefix and the namespace URI.
    """
    if len(root.nsmap) == 0:
        return "", {}
    return "onix:", {"onix": root.nsmap[list(root.nsmap.keys())[0]]}


def match_from_xpaths(element, xpaths, NS_PREFIX, NS):
    """
    It takes an element, a list of xpaths, and a namespace prefix and namespace, and returns the first
    element that matches any of the xpaths

    :param element: the element to search
    :param xpaths: a list of xpaths to try to match against
    :param NS_PREFIX: The namespace prefix for the XML document
    :param NS: the namespace dictionary
    :return: The first element that matches the xpath.
    """
    for xpath_tempate in xpaths:
        xpath = xpath_tempate.format(NS_PREFIX=NS_PREFIX)
        res = folded_xpath(element, xpath, NS)
        if len(res) > 0:
            return res[0]

    return None


def cleanup_doi(doi):
    """
    An exampe of a post processing function.

    :param doi: The DOI of the article
    :return: the lowercase version of the doi.
    """
    return doi.lower()


def cleanup_isbn(isbn):
    """
    An exampe of a post processing function.

    :param isbn: The ISBN of the book
    :return: the ISBN stripped of dashes.
    """

    return isbn.replace("-", "").replace(" ", "")

def cleanup_titlewithoutprefix(titlewithoutprefix):
    logger.info(">>>>> Cleaning up titlewithoutprefix")
    return titlewithoutprefix

def cleanup_row(row):
    if row['Title'] is None:
        row['Title'] = row['TitlePrefix'] + ' ' + row['TitleWithoutPrefix'] 
    return row


def cleanup_element(element_name, element_value):
    """
    If there is a function called `cleanup_<element_name>` in the global namespace, call it with the
    element value as the argument

    :param element_name: The name of the element that we're cleaning up
    :param element_value: The value of the element that we're cleaning up
    :return: the value of the element after it has been cleaned up.
    """
    if element_value is None:
        return None
    func_name = f"cleanup_{element_name.lower()}"
    if func_name in globals():
        logger.info(
            f"Calling post processing function {func_name} for element {element_name} with value {element_value}"
        )
        return globals()[func_name](element_value)
    else:
        logger.info(
            f"No post processing function {func_name} for element {element_name}"
        )
        return element_value


def onix_to_df(xml):
    """
    > For each record in the XML file, find the value of each field of interest, and add it to a row in
    a dataframe

    :param xml: the xml file as a string
    :return: A dataframe with the columns specified in XPATHS_OF_INTEREST
    """

    tree = etree.parse(BytesIO(xml))
    root = tree.getroot()
    NS_PREFIX, NS = get_ns_dec(root)

    logger.debug(f"Namespace declaration is:{NS_PREFIX}")
    logger.debug(f"Namespaces are: {NS}")

    df = pd.DataFrame(columns=[entry["name"] for entry in XPATHS_OF_INTEREST])

    for record in folded_xpath(root, f"{NS_PREFIX}Product", NS):
        row = {}
        for entry in XPATHS_OF_INTEREST:
            res = match_from_xpaths(record, entry["l_xpaths"], NS_PREFIX, NS)
            if res is None:
                res = match_from_xpaths(record, entry["s_xpaths"], NS_PREFIX, NS)
            res = cleanup_element(entry["name"], res)
            row[entry["name"]] = res
        row = cleanup_row(row)
        df = pd.concat([df, pd.DataFrame([row])], ignore_index=True)

    
    return df


def read_xml(filename):
    return open(filename, "rb").read()


def main(filename: str = typer.Argument(..., help="XML file to read")):
    logger.info(f"Reading file: {filename}")
    df = onix_to_df(read_xml(filename))
    #print(prettify(df))
    print(df)


# create dataframe from xml file
if __name__ == "__main__":
    typer.run(main)
