## Onix Wrangler

![Image](https://crossref.org/img/labs/creature2.svg)
### Warnings, Caveats and Weasel Words

This is a [Crossref Labs](https://www.crossref.org/labs/) proof of concept.

It is untested, unruly, and it will almost certainly chew on your metadata and soil your floor.

## What is this?

A proof of concept to see if we can make sense of [ONIX for books](https://en.wikipedia.org/wiki/ONIX_for_Books) and convert it into more tractable representations.

It allows you to upload a set of ONIX files, it will then extract a subset of elements from the files and let you re-download the data as CSV or JSON.

## Why?

Even though [ONIX for books](https://en.wikipedia.org/wiki/ONIX_for_Books) is a "standard" for book metadata. There appears to be very little good open source tooling for processing ONIX metadata.

This must be a massive hurdle for small book publishers.

And it might be cool if Crossref were able to accept ONIX files for deposit.

ONIX is also a PITA. There are two common versions of ONIX and both are exteremely tedious to process. To make things worse, different publishers use ONIX in wildly eccentric ways.

This POC tries to deal as gracefully as possible with some of these eccentricities.

## What next?

This POC only shows half a pipeline. It parses and converts ONIX into an intermediate representation that can then be spat-out in CSV or JSON.

The next step would be to show the conversion from the intermediate representation back into a different XML schema. Like the [Crossref schema](https://www.crossref.org/documentation/schema-library/), or ONIX suitable for registering with [OAPEN](https://www.oapen.org/).

## Code

[The code is available under an MIT license on GitLab](https://gitlab.com/crossref/labs/onix-wrangler).

## Aknowledgements

Thanks much to [OAPEN](https://www.oapen.org/) who provided us with some example [VBA](https://en.wikipedia.org/wiki/Visual_Basic_for_Applications) scripts (for reference) as well as some sample ONIX files (to test against).