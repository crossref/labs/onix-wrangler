# Describe the XPaths of interest.
# Include the name of the field, the XPaths for the long version of the element,
# and the XPaths for the short version of the element. Evaluation of the XPaths
# will be attempted in the order they are listed until a value is found.
# The value will be stored in a dataframe column field with the name given.
XPATHS_OF_INTEREST = [
    {
        "name": "ISBN",
        "l_xpaths": [
            "{NS_PREFIX}ProductIdentifier[{NS_PREFIX}ProductIDType=15]/{NS_PREFIX}IDValue/text()",
            "{NS_PREFIX}WorkIdentifier[{NS_PREFIX}WorkIDType=15]/{NS_PREFIX}IDValue/text()",
        ],
        "s_xpaths": [
            "{NS_PREFIX}productidentifier[{NS_PREFIX}b221=15]/{NS_PREFIX}b244/text()",
        ],
    },
    {
        "name": "DOI",
        "l_xpaths": [
            "{NS_PREFIX}ProductIdentifier[{NS_PREFIX}ProductIDType=06]/{NS_PREFIX}IDValue/text()",
            "{NS_PREFIX}RelatedMaterial/{NS_PREFIX}RelatedWork/{NS_PREFIX}WorkIdentifier[{NS_PREFIX}WorkIDType=06]/{NS_PREFIX}IDValue/text()",
            "{NS_PREFIX}WorkIdentifier[{NS_PREFIX}WorkIDType=06]/{NS_PREFIX}IDValue/text()",
        ],
        "s_xpaths": [
            "{NS_PREFIX}productidentifier[{NS_PREFIX}b221=06]/{NS_PREFIX}b244/text()",
        ],
    },
    {
        "name": "CCLicense",
        "l_xpaths": [
            "{NS_PREFIX}DescriptiveDetail/{NS_PREFIX}EpubLicense/{NS_PREFIX}EpubLicenseExpression/{NS_PREFIX}EpubLicenseExpressionLink/text()",
            "{NS_PREFIX}DescriptiveDetail/{NS_PREFIX}EpubLicense/{NS_PREFIX}EpubLicenseName/text()",
        ],
        "s_xpaths": [
            "{NS_PREFIX}descriptivedetail/{NS_PREFIX}epublicense/{NS_PREFIX}epublicenseexpression/{NS_PREFIX}x510/text()",
            "{NS_PREFIX}othertext[{NS_PREFIX}d102=46]/{NS_PREFIX}d104/text()",
            "{NS_PREFIX}othertext[{NS_PREFIX}d102=46]/{NS_PREFIX}d106/text()",
        ],
    },
    {
        "name": "SeriesTitle",
        "l_xpaths": [
            "{NS_PREFIX}DescriptiveDetail/{NS_PREFIX}TitleDetail/{NS_PREFIX}TitleElement/{NS_PREFIX}TitleText/text()",
            "{NS_PREFIX}Series/{NS_PREFIX}TitleOfSeries/text()",
        ],
        "s_xpaths": [
            "{NS_PREFIX}descriptivedetail/{NS_PREFIX}titledetail/{NS_PREFIX}titleelement/{NS_PREFIX}b203/text()",
            "{NS_PREFIX}series/{NS_PREFIX}b018/text()",
        ],
    },
    {
        "name": "SeriesNumber",
        "l_xpaths": [
            "{NS_PREFIX}DescriptiveDetail/{NS_PREFIX}Collection/{NS_PREFIX}TitleDetail/{NS_PREFIX}TitleElement/{NS_PREFIX}PartNumber/text()",
            "{NS_PREFIX}DescriptiveDetail/{NS_PREFIX}Collection/{NS_PREFIX}CollectionSequence/{NS_PREFIX}CollectionSequenceNumber/text()",
            
        ],
        "s_xpaths": [
            "{NS_PREFIX}descriptivedetail/{NS_PREFIX}collection/{NS_PREFIX}titledetail/{NS_PREFIX}titleelement/{NS_PREFIX}x410/text()",
            "{NS_PREFIX}descriptivedetail/{NS_PREFIX}collection/{NS_PREFIX}collectionsequence/{NS_PREFIX}x481/text()",
            "{NS_PREFIX}series/{NS_PREFIX}b019/text()",
        ],
    
    },
    {
        "name": "TitlePrefix",
        "l_xpaths": [
            "{NS_PREFIX}DescriptiveDetail/{NS_PREFIX}TitleDetail/{NS_PREFIX}TitleElement/{NS_PREFIX}TitlePrefix/text()",
        ],
        "s_xpaths": [
            "{NS_PREFIX}descriptivedetail/{NS_PREFIX}titledetail/{NS_PREFIX}titleelement/{NS_PREFIX}b030/text()",
        ],
    },
    {
        "name": "TitleWithoutPrefix",
        "l_xpaths": [
            "{NS_PREFIX}DescriptiveDetail/{NS_PREFIX}TitleDetail/{NS_PREFIX}TitleElement/{NS_PREFIX}TitleWithoutPrefix/text()",
        ],
        "s_xpaths": [
            "{NS_PREFIX}descriptivedetail/{NS_PREFIX}titledetail/{NS_PREFIX}titleelement/{NS_PREFIX}b031/text()",
        ],
    },
    {
        "name": "Title",
        "l_xpaths": [
            "{NS_PREFIX}DescriptiveDetail/{NS_PREFIX}TitleDetail/{NS_PREFIX}TitleElement/{NS_PREFIX}TitleText/text()",
            "{NS_PREFIX}Title/{NS_PREFIX}TitleText/text()",
        ],
        "s_xpaths": [
            "{NS_PREFIX}descriptivedetail/{NS_PREFIX}titledetail/{NS_PREFIX}titleelement/{NS_PREFIX}b203/text()",
            "{NS_PREFIX}title/{NS_PREFIX}b203/text()",
        ],
        
    },
    
]
